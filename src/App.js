import React, { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
// import { mockData } from "./utils"
import axiosFetch from "./utils/axios";
import { GET_DATA } from "./utils/urls"
import { useSelector, useDispatch } from "react-redux";

import { setSearchData } from "./store/actionCreators";
import { getSearchData } from "./store/selectors"

import "./styles.css"

const App = () => {

  const [searchText, setSearchText] = useState("")

  const dispatch = useDispatch();
  useEffect(() => {

    const getData = () => {
      const dataContent = axiosFetch(GET_DATA);
      Promise.resolve(dataContent).then((arrList) => {
        dispatch(setSearchData(arrList))
      });
    };

    getData();
  }, [dispatch]);

  const filterText = (items, text) => {
    return items.filter((item) => {
      return item.toLowerCase().indexOf(text.toLowerCase()) > -1;
    });
  }

  const searchList = useSelector(getSearchData);
  const visibleItems = filterText(searchList, searchText)

  const showResultContainer = searchText.length > 0 && visibleItems.length !== 0
  const showNotFindContainer = visibleItems.length === 0 && searchText !== "" && <h2>No results found</h2>

  

  const renderResultContainer = () => {
    const content = visibleItems.map((item) => {
      return (
        <div className="filter-line">
          {item}
        </div>
      )
    })

    return content
  }


  return (
    <div className="app-container">
      <TextField
        id="outlined-basic"
        label="Search..."
        className="search-field"
        value={searchText}
        variant="outlined"
        onChange={(e) => setSearchText(e.target.value)}
      />

      {showResultContainer ?
        <div className="result-content">
          {renderResultContainer()}
        </div>
        : showNotFindContainer}

    </div>
  );
}

export default App;
