import React from 'react';

import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from "react-redux";
import ReactDOM from 'react-dom/client';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from "history";
import Store from "./store";

const history = createBrowserHistory();

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={Store}>
    <Router history={history} basename='/'>
      <App />
    </Router>
  </Provider>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
