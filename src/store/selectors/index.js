const getDataReducerState = (state) => {
    return state?.GetDataReducer;
};

export const getSearchData = (state) => {
    return getDataReducerState(state).searchData;
}; 