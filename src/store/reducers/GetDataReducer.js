import {
    SET_SEARCH_DATA,
} from "../actions";

const initialState = {
    searchData: [],
};

export default  function Reducers (state = initialState, action) {
    switch (action.type) {
        case SET_SEARCH_DATA:
            return Object.assign({}, {
                ...state,
                searchData: action.payload,
            });
        default:
            return state;
    }
}
