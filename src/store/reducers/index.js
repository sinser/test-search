import { combineReducers } from "redux";
import GetDataReducer from "./GetDataReducer";

const rootReducer = combineReducers({
    GetDataReducer
});

export default rootReducer;
