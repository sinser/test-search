import {
    SET_SEARCH_DATA,
  } from "../actions";
  
  export const setSearchData = (searchData) => {
    return { type: SET_SEARCH_DATA, payload: searchData };
  };